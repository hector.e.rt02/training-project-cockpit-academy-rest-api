import { Router } from "express";
import { getWorkspaces,getProjects,getReportDetailed } from "../controllers/clockify";

const router = Router();

router.post('/Workspaces',getWorkspaces);

router.post('/Projects',getProjects);

router.post('/Reports/Detailed',getReportDetailed);

export default router;