import { Router } from "express";
import { registerUser,editMyProfile,forgotPassword,resetPassword,login } from "../controllers/user";

const router = Router();

router.post('/RegisterUser', registerUser);

router.post('/EditMyProfile', editMyProfile);

router.post('/ForgotPassword', forgotPassword);

router.post('/ResetPassword', resetPassword);

router.post('/Login/:userName/:password', login);

export default router;