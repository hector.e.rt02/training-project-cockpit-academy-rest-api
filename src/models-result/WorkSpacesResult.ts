import { Workspace } from "../models/clockify/workspace";
import { StatusResult } from "./StatusResult";

export class WorkspacesResult{
    constructor(public Workspaces:Array<Workspace>,
    public Status:StatusResult){
        this.Workspaces = Workspaces;
        this.Status = Status;
    }
}