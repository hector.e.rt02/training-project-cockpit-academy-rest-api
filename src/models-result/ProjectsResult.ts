import { Project } from "../models/clockify/Project";
import { StatusResult } from "./StatusResult";

export class ProjectsResult{
    constructor(public Projects:Project[],
    public Status:StatusResult){
        this.Projects = Projects;
        this.Status = Status;
    }
}