import { Report } from "../models/clockify/Report";
import { StatusResult } from "./StatusResult";

export class ReportResult{
    constructor(public ReportRequired:Report,
    public Status:StatusResult) {
        this.ReportRequired = ReportRequired;
        this.Status = Status;
    }
}