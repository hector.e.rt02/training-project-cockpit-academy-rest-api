import { User } from "../models/User";
import { StatusResult } from "./StatusResult";

export class LoginResult{
    constructor(public UserLogged:User,
    public SessionToken:string,
    public Status:StatusResult){
        this.UserLogged = UserLogged;
        this.SessionToken = SessionToken;
        this.Status = Status;
    }
}