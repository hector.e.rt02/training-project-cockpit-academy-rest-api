export enum StatusCode{
    OK=200,
    CREATED=201,
    ACEPTED=202,
    NO_CONTENT=204,
    NOT_MODIFIED=304,
    BAD_REQUEST=400,
    UNAUTHORIZED=401,
    INTERNAL_SERVER_ERROR=500,
    NOT_UPDATED=512
}

export class StatusResult{
    public Code:StatusCode;
    public Description:string;
    public Message:string;

    constructor(pCode?:StatusCode, pMessage?:string){
        this.Code = pCode != null ? pCode : StatusCode.BAD_REQUEST;
        this.Description=StatusCode[pCode != null ? pCode : StatusCode.BAD_REQUEST];
        this.Message = pMessage != null ? pMessage : '';
    }
}