import { sqlConfig, SqlParameter } from "../utils/database-config";
import sql from "mssql";

export default function excuteScript(sqlScript:string,parameters:SqlParameter[]=[],isUSP:boolean=false){
    return sql.connect(sqlConfig).then(pool => {
        let req = pool.request();

        parameters.forEach(p => {
            req.input(p.name,p.type,p.value);
        });
        
        if (isUSP) {
            return req.execute(sqlScript);
        } else {
            return req.query(sqlScript);
        }
    }).then(result => {
        // console.dir(result)
        return result;
    })
}