import * as dotenv from "dotenv";
import sql from "mssql";

dotenv.config();

export const sqlConfig:sql.config = {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    server: process.env.DB_SERVER!
    ,
    pool: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 30000
    },
    options: {
      encrypt: false, // for azure
      trustServerCertificate: false // change to true for local dev / self-signed certs
    }
  }

  export interface SqlParameter{
    name:string,
    type:sql.ISqlTypeFactoryWithNoParams,
    value:any
  }