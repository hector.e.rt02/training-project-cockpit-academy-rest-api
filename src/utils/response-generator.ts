import { StatusCode, StatusResult } from "../models-result/StatusResult";
import { RequestHandler,response,Response } from "express";

export default function ResponseGenerator(res:Response,Status:StatusResult, Data?:any){
    let resAux:Response = res;

    resAux.status(StatusCode.OK).json(Data != null ? Data : Status);

    return resAux;
}