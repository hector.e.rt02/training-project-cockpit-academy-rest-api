import { IReportDetailedClockify } from "./IReportDetailedClockify";

export interface IReportDetailed extends IReportDetailedClockify{
    clockifyToken:string,
    workspaceId:string
}