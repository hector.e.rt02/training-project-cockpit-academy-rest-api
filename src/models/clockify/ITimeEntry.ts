export interface ITimeEntry{
    _id:string,
    description:string,
    userId:ShadowRootInit,
    billable:boolean,
    projectId:any,
    timeInterval:{start:Date,end:Date,duration:number},
    userName:string,
    userEmail:string,
    projectName:string,
    projectColor:string,
    clientName:string,
    clientId: string
}