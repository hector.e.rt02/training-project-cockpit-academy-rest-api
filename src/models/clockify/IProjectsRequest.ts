import { IProjectsRequestClockify } from "./IProjectsRequestClockify";

export interface IProjectsRequest extends IProjectsRequestClockify{    
    clockifyToken:string,
    workspaceId:string
}