export interface IReportDetailedClockify{    
    // REQUIRED
    dateRangeStart: Date,
    dateRangeEnd: Date,
    detailedFilter: {
        page: number,
        pageSize: number
    }
    amountShown: "HIDE_AMOUNT" | "EARNED" | "COST" | "PROFIT"
}
