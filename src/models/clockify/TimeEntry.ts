import { ITimeEntry } from "./ITimeEntry";

export class TimeEntry implements ITimeEntry{
    constructor(public _id:string,
        public description:string,
        public userId:ShadowRootInit,
        public billable:boolean,
        public projectId:any,
        public timeInterval:{start:Date,end:Date,duration:number},
        public userName:string,
        public userEmail:string,
        public projectName:string,
        public projectColor:string,
        public clientName:string,
        public clientId: string){
            this._id =_id;
            this.description =description;
            this.userId =userId;
            this.billable =billable;
            this.projectId =projectId;
            this.timeInterval =timeInterval;
            this.userName =userName;
            this.userEmail =userEmail;
            this.projectName =projectName;
            this.projectColor =projectColor;
            this.clientName =clientName;
            this.clientId =clientId;
        }
}