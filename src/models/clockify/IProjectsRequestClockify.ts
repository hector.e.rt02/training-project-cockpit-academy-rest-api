export interface IProjectsRequestClockify{
    archived:boolean,
    name:string,
    page:number,
    pageSize:number,
    billable:boolean,
    clients:string[],
    containsClient:boolean,
    clientStatus:string,
    users:string[],
    containsUsers:boolean,
    userStatus:string,
    isTemplate:boolean,
    sortColumn:string,
    sortOrder:string
}