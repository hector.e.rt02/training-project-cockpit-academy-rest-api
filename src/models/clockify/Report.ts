import { TimeEntry } from "./TimeEntry";
import { TotalReport } from "./TotalReport";

export class Report{
    constructor(public totals?:TotalReport[],
        public timeentries?:TimeEntry[]) {
        this.totals = totals;
        this.timeentries = timeentries;
    }
}