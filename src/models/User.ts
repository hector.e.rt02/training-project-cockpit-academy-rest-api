import { StatusResult, StatusCode } from "../models-result/StatusResult";
import { IsNotEmpty,Length,IsNumber } from "class-validator";
import { LoginResult } from "../models-result/LoginResult";
import  excuteScript  from "../Database/database-server";
import { SqlParameter } from "../utils/database-config";
import sql from "mssql";
import { json } from "stream/consumers";
export enum NotificationType{
    RESET_PASSWORD=1,
    CONFIRM_ACCOUNT
}

export class User{
    @IsNumber()
    public UserId:number;
    @Length(3,15)
    public UserName:string;    
    @Length(5,20)
    public Password:string;
    @Length(5,200)
    public Name:string;
    @Length(10,100)
    public Email:string;
    @IsNotEmpty()
    public Photo:string;
    @IsNotEmpty()
    public ClockifyToken:string;

    constructor(pUserId:number,
    pUserName:string,
    pPassword:string,
    pName:string,
    pEmail:string,
    pPhoto:string,
    pClockifyToken:string){
        this.UserId = pUserId;
        this.UserName = pUserName;
        this.Password = pPassword;
        this.Name = pName;
        this.Email = pEmail;
        this.Photo = pPhoto;
        this.ClockifyToken = pClockifyToken;
    }

    //#region Static Methods
    public static register(pUser:User){
        // this.UpdatePruebasInfo(pUser,true);
        
        let par:Array<SqlParameter> =[];
        par.push({name:"pName",type:sql.VarChar,value:pUser.Name})
        par.push({name:"pEmail",type:sql.VarChar,value:pUser.Email})
        par.push({name:"pUserName",type:sql.VarChar,value:pUser.UserName})
        par.push({name:"pPassword",type:sql.VarChar,value:pUser.Password})
        par.push({name:"pPhoto",type:sql.VarChar,value:pUser.Photo})
        par.push({name:"pClockifyToken",type:sql.VarChar,value:pUser.ClockifyToken})

        return excuteScript("uspUserInsert",par,true).then((result)=>{
            let res:StatusResult;

            if (result.recordset.length > 0 && result.recordset[0].UserId) {
                res= new StatusResult(StatusCode.OK,JSON.stringify({UserId: +result.recordset[0].UserId}));
            } else {
                res= new StatusResult(StatusCode.NOT_MODIFIED,"We couldn't save your info.");
            }

            return res;
        }).catch(msj => {
            return new StatusResult(StatusCode.NOT_MODIFIED,JSON.stringify(msj));
        })        
    }

    public static login(pUserName:string,pPassword:string){
        let result:LoginResult;

        // if (pUserName == pruebas.UserName && pPassword == pruebas.Password) {
        //     result = new LoginResult(pruebas,"hola",new StatusResult(StatusCode.OK))
        // } else {
        //     result = new LoginResult(new User(0,pUserName,pPassword,"","","",""),"",new StatusResult(StatusCode.NO_CONTENT));
        // }

        result = new LoginResult(new User(0,pUserName,pPassword,"","","",""),"",new StatusResult(StatusCode.NO_CONTENT));
        return result;
    }

    public static editMyProfile(pUser:User){
        // let result:StatusResult;
        // if (pUser.UserId == pruebas.UserId) {        
        //     this.UpdatePruebasInfo(pUser);

        //     result = new StatusResult(StatusCode.OK)
        // } else {            
        //     return new StatusResult(StatusCode.UNAUTHORIZED);
        // }
        // return result;

        return new StatusResult(StatusCode.UNAUTHORIZED);
    }

    public static forgotPassword(pEmail:string){
        return new StatusResult(StatusCode.NO_CONTENT);
    }

    public static resetPassword(pUserId:number,pOldPassword:string,pNewPassword:string){
        // if (pruebas.UserId == pUserId && pruebas.Password == pOldPassword ) {
        //     pruebas.Password = pNewPassword;

        //     return new StatusResult(StatusCode.OK,JSON.stringify(pruebas));
        // } else {
        //     return new StatusResult(StatusCode.NOT_MODIFIED);
        // }
        return new StatusResult(StatusCode.NOT_MODIFIED);
    }

    public static sendEmailNotification(pEmail:string,pType:NotificationType){
        return new StatusResult(StatusCode.NO_CONTENT);
    }

    // private static UpdatePruebasInfo(pUser:User, updateId:boolean=false){
    //     if (updateId) {
    //         pruebas.UserId=pUser.UserId;
    //     }
        
    //     pruebas.UserName = pUser.UserName;
    //     pruebas.Photo = pUser.Photo;
    //     pruebas.Email = pUser.Email;
    //     pruebas.ClockifyToken = pUser.ClockifyToken;
    // }

    //#endregion Static Methods
}

// const pruebas:User = new User(2,"juaniquito01","123456","Juan Perez","juaniquito-el-real@gmail.com","","");