import { IsNumber,Min,Length } from "class-validator";

export class ResetPasswordRequest{
    @IsNumber()
    @Min(1)
    public UserId:number;
    @Length(5,20)
    public OldPassword:string;
    @Length(5,20)
    public NewPassword:string;

    constructor(pUserId:number,pOldPassword:string,pNewPassword:string){
        this.UserId = pUserId;
        this.OldPassword = pOldPassword;
        this.NewPassword = pNewPassword;
    }
}