import { NextFunction, RequestHandler,Request,Response } from "express";
import { NotificationType, User } from "../models/User";
import { validate } from "class-validator";
import ResponseGenerator from "../utils/response-generator";
import { StatusCode, StatusResult } from "../models-result/StatusResult";
import { isNullOrUndefined } from "util";
import { ResetPasswordRequest } from "../models/reset-password-request";
import { LoginResult } from "../models-result/LoginResult";

//#region Controller Methods
export const login: RequestHandler = (req:Request,res:Response,next:NextFunction) =>{
    try {
        let userName:string = req.params["userName"];
        let password:string = req.params["password"];

        let result:LoginResult;

        if (userName==null || password == null) {
            result = new LoginResult(new User(0,"","","","","",""),"",new StatusResult(StatusCode.BAD_REQUEST,"'userName' and 'password' fields are required."));
        } else {
            result = User.login(userName,password);
        }

        return ResponseGenerator(res, result.Status,result);
    } catch (error) {
        return ResponseGenerator(res,new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(error)))
    }
}

export const registerUser: RequestHandler = (req:Request,res:Response,next:NextFunction) =>{
    let user:User = req.body;
    return validate(user).then(async (errors) => {
        let result:StatusResult;
        if (errors.length > 0) {
            result = new StatusResult(StatusCode.BAD_REQUEST,JSON.stringify(errors));
        } else {
           result = await User.register(user);
        }

        return ResponseGenerator(res,result);
    }).catch(msj => {
        return ResponseGenerator(res,new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(msj)))
    })
}

export const editMyProfile: RequestHandler =(req:Request,res:Response,next:NextFunction) =>{
    let user:User = req.body;
    return validate(user).then(erros => {
        let result:StatusResult;

        if (erros.length > 0) {
            result = new StatusResult(StatusCode.BAD_REQUEST,JSON.stringify(erros));
        } else {
            result = User.editMyProfile(user);
        }

        return ResponseGenerator(res,result);
    }).catch(msj => {
        return ResponseGenerator(res,new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(msj)))
    })
}

export const forgotPassword: RequestHandler = (req:Request,res:Response,next:NextFunction) =>{
    try {
        let email:string = (req.body as {email:string}).email;
        let result:StatusResult;

        if (isNullOrUndefined(email)) {
            result = new StatusResult(StatusCode.BAD_REQUEST,"The field 'Email' is required.");
        } else {
            result = User.forgotPassword(email);
        }

        return ResponseGenerator(res,result);
    } catch (error) {
        return ResponseGenerator(res,new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(error)))
    }
}

export const resetPassword: RequestHandler = (req:Request,res:Response,next:NextFunction)=>{
    let data:ResetPasswordRequest= req.body;
    console.log(`Data: ${JSON.stringify(data)}`);

    return validate(data).then(errors =>{
        let result:StatusResult;

        if (errors.length > 0) {
            result = new StatusResult(StatusCode.BAD_REQUEST,JSON.stringify(errors));
        } else {
            result = User.resetPassword(data.UserId,data.OldPassword,data.NewPassword);
        }

        return ResponseGenerator(res, result);
    }).catch(msj => {
        return ResponseGenerator(res,new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(msj)))
    })
}

export const sendEmailNotification: RequestHandler = (req:Request,res:Response,next:NextFunction)=>{
    try {
        let data = (req.body as {email:string,type:NotificationType});
        
        if (isNullOrUndefined(data)) {
            let responseData = {message:"The fields 'Email' and 'type' are required.",dataRecived: req.body};
            let msj = "The information received is not valid";

            return ResponseGenerator(res, new StatusResult(StatusCode.BAD_REQUEST,msj),responseData);
        } else {
            return ResponseGenerator(res, User.sendEmailNotification(data.email,data.type));
        }
    } catch (error) {
        return ResponseGenerator(res,new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(error)))
    }
}
//#endregion Controller Methods