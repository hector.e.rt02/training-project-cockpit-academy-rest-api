import { NextFunction, RequestHandler,Request,Response } from "express";
import { StatusCode, StatusResult } from "../models-result/StatusResult";
import { WorkspacesResult } from "../models-result/WorkSpacesResult";
import ResponseGenerator from "../utils/response-generator";
import reqLib from "request";
import * as dotenv from "dotenv";
import { Workspace } from "../models/clockify/workspace";
import { IProjectsRequest } from "../models/clockify/IProjectsRequest";
import { IProjectsRequestClockify } from "../models/clockify/IProjectsRequestClockify";
import { ProjectsResult } from "../models-result/ProjectsResult";
import { Project } from "../models/clockify/Project";
import { IReportDetailed } from "../models/clockify/IReportDetailed";
import { IReportDetailedClockify } from "../models/clockify/IReportDetailedClockify";
import { ReportResult } from "../models-result/ReportResult";
import { Report } from "../models/clockify/Report";
import { TimeEntry } from "../models/clockify/TimeEntry";
import { TotalReport } from "../models/clockify/TotalReport";
dotenv.config();

export const getWorkspaces: RequestHandler = (req:Request,res:Response,next:NextFunction) =>{
    try {
        let clockifyToken:string = (req.body as {clockifyToken:string}).clockifyToken;
        let url = `${process.env.CLOCKIFY_BASE_ENDPOINT!}/workspaces`;

        let options = {
            url: url,
            method: "GET",
            headers: {
                "content-type":"application/json",
                "X-Api-Key":clockifyToken
            }
          };

        return reqLib(options, (error, response, body) => {
            let result:WorkspacesResult;
            let data:any[]= JSON.parse(body);

            if (data.length > 0) {
                let ws:Workspace[]=[];
                data.forEach(workspace => {
                    ws.push(new Workspace(workspace.id,workspace.name))
                });
                result = new WorkspacesResult(ws,new StatusResult(StatusCode.OK));
            } else {
                result=new WorkspacesResult([],new StatusResult(StatusCode.NO_CONTENT))   
            }

            return ResponseGenerator(res, result.Status,result);
        })        
    } catch (error) {
        let result = new WorkspacesResult([],new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(error)));
        return ResponseGenerator(res, result.Status,result);
    }
}

export const getProjects: RequestHandler = (req:Request,res:Response,next:NextFunction) =>{
    try {
        let bodyData = (req.body as IProjectsRequest);
        let clockifyToken:string = bodyData.clockifyToken;
        let url = `${process.env.CLOCKIFY_BASE_ENDPOINT!}/workspaces/${bodyData.workspaceId}/projects`;
        let query = (bodyData as IProjectsRequestClockify);

        let options = {
            url: url,
            method: "GET",
            headers: {
                "content-type":"application/json",
                "X-Api-Key":clockifyToken
            },
            qs: query
          };

        return reqLib(options, (error, response, body) => {
            let result:ProjectsResult;

            if (error) {
                result = new ProjectsResult([],new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(error)));
                return ResponseGenerator(res, result.Status,result);
            } else {
                let data:any[]= JSON.parse(body);
                let projects:Project[] =[];

                if (data.length > 0) {
                    data.forEach(project => {
                        projects.push(new Project(project.id,project.name,project.workspaceId));
                    });

                    result = new ProjectsResult(projects,new StatusResult(StatusCode.OK));
                } else {
                    result = new ProjectsResult([],new StatusResult(StatusCode.NO_CONTENT));
                }

                return ResponseGenerator(res, result.Status,result);
            }            
        })        
    } catch (error) {
        let result = new WorkspacesResult([],new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(error)));
        return ResponseGenerator(res, result.Status,result);
    }
}

export const getReportDetailed: RequestHandler = (req:Request,res:Response,next:NextFunction) =>{
    try {
        let bodyData = (req.body as IReportDetailed);
        let clockifyToken:string = bodyData.clockifyToken;
        let url = `${process.env.CLOCKIFY_REPORT_BASE_ENDPOINT!}/workspaces/${bodyData.workspaceId}/reports/detailed`;
        
        delete req.body.clockifyToken
        delete req.body.workspaceId
        let clockifyBody:IReportDetailedClockify = (req.body as IReportDetailedClockify);

        let options = {
            url: url,
            method: "POST",
            headers: {
                "content-type":"application/json",
                "X-Api-Key":clockifyToken
            }, 
            body:JSON.stringify(clockifyBody)
          };

        return reqLib(options, (error, response, body) => {
            let result:ReportResult;
            let report = new Report();

            if (error) {
                result = new ReportResult(new Report(),new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(error)));
                return ResponseGenerator(res, result.Status,result);
            } else {
                let data:any= JSON.parse(body);
                let timeEntries:TimeEntry[]=[];
                let totales:TotalReport[]=[];

                if (data && data.totals.length > 0 && data.timeentries.length > 0) {
                    data.totals.forEach((tt:any) => {
                        totales.push({_id:tt._id,totalTime:tt.totalTime,totalBillableTime:tt.totalBillableTime,entriesCount:tt.entriesCount,totalAmount:tt.totalAmount});
                    });

                    data.timeentries.forEach((te:any) => {
                        timeEntries.push(new TimeEntry(te._id,te.description,te.userId,te.billable,te.projectId,te.timeInterval,te.userName,
                            te.userEmail,te.projectName,te.projectColor,te.clientName,te.clientId));
                    });

                    result = new ReportResult(new Report(totales,timeEntries),new StatusResult(StatusCode.OK));
                } else {
                    result = new ReportResult(new Report(),new StatusResult(StatusCode.NO_CONTENT));
                }

                return ResponseGenerator(res, result.Status,result);
            }
        })        
    } catch (error) {
        let result = new WorkspacesResult([],new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,JSON.stringify(error)));
        return ResponseGenerator(res, result.Status,result);
    }
}