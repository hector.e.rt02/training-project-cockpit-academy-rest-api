import express, {NextFunction, Request,Response} from 'express';
import { json } from "body-parser";
import userRouters from './routes/user-routes'
import clockifyRouters from './routes/clockify-routes'
import { StatusCode, StatusResult } from './models-result/StatusResult';
import * as dotenv from "dotenv";
dotenv.config();

const app = express();

app.use(json());

app.use('/user',userRouters);
app.use('/clockify',clockifyRouters);

app.use((err:Error,req:Request,res:Response,next:NextFunction) =>{
   res.status(500).json(new StatusResult(StatusCode.INTERNAL_SERVER_ERROR,err.message));
});

app.listen(process.env.SERVER_PORT);