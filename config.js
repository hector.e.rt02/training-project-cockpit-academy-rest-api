const dotenv = require("dotenv").config();

module.exports = {
    DB_USER=process.env.DB_USER,
    DB_NAME=process.env.DB_NAME,
    DB_PASSWORD=process.env.DB_PASSWORD,
    DB_SERVER=process.env.DB_SERVER,
    SERVER_PORT=process.env.SERVER_PORT  
}