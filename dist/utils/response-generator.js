"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const StatusResult_1 = require("../models-result/StatusResult");
function ResponseGenerator(res, Status, Data) {
    let resAux = res;
    resAux.status(StatusResult_1.StatusCode.OK).json(Data != null ? Data : Status);
    return resAux;
}
exports.default = ResponseGenerator;
