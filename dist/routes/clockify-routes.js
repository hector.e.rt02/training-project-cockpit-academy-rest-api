"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const clockify_1 = require("../controllers/clockify");
const router = (0, express_1.Router)();
router.post('/Workspaces', clockify_1.getWorkspaces);
router.post('/Projects', clockify_1.getProjects);
router.post('/Reports/Detailed', clockify_1.getReportDetailed);
exports.default = router;
