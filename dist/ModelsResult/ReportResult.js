"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportResult = void 0;
const Report_1 = require("../models/Report");
const StatusResult_1 = require("./StatusResult");
class ReportResult {
    /**
     *
     */
    constructor(ReportRequired, Status) {
        this.ReportRequired = ReportRequired;
        this.Status = Status;
        this.ReportRequired = ReportRequired != null ? ReportRequired : new Report_1.Report();
        this.Status = Status != null ? Status : new StatusResult_1.StatusResult();
    }
}
exports.ReportResult = ReportResult;
