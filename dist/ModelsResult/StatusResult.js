"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusResult = exports.StatusCode = void 0;
var StatusCode;
(function (StatusCode) {
    StatusCode[StatusCode["OK"] = 200] = "OK";
    StatusCode[StatusCode["CREATED"] = 201] = "CREATED";
    StatusCode[StatusCode["ACEPTED"] = 202] = "ACEPTED";
    StatusCode[StatusCode["NO_CONTENT"] = 204] = "NO_CONTENT";
    StatusCode[StatusCode["NOT_MODIFIER"] = 304] = "NOT_MODIFIER";
    StatusCode[StatusCode["BAD_REQUEST"] = 400] = "BAD_REQUEST";
    StatusCode[StatusCode["UNAUTHORIZED"] = 401] = "UNAUTHORIZED";
    StatusCode[StatusCode["INTERNAL_SERVER_ERROR"] = 500] = "INTERNAL_SERVER_ERROR";
    StatusCode[StatusCode["NOT_UPDATED"] = 512] = "NOT_UPDATED";
})(StatusCode = exports.StatusCode || (exports.StatusCode = {}));
class StatusResult {
    constructor(Code, Message) {
        this.Code = Code;
        this.Message = Message;
        this.Code = Code != null ? Code : StatusCode.BAD_REQUEST;
        this.Description = StatusCode[Code != null ? Code : StatusCode.BAD_REQUEST];
        this.Message = Message != null ? Message : '';
    }
}
exports.StatusResult = StatusResult;
