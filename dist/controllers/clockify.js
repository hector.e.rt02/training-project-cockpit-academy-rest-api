"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getReportDetailed = exports.getProjects = exports.getWorkspaces = void 0;
const StatusResult_1 = require("../models-result/StatusResult");
const WorkSpacesResult_1 = require("../models-result/WorkSpacesResult");
const response_generator_1 = __importDefault(require("../utils/response-generator"));
const request_1 = __importDefault(require("request"));
const dotenv = __importStar(require("dotenv"));
const workspace_1 = require("../models/clockify/workspace");
const ProjectsResult_1 = require("../models-result/ProjectsResult");
const Project_1 = require("../models/clockify/Project");
const ReportResult_1 = require("../models-result/ReportResult");
const Report_1 = require("../models/clockify/Report");
const TimeEntry_1 = require("../models/clockify/TimeEntry");
dotenv.config();
const getWorkspaces = (req, res, next) => {
    try {
        let clockifyToken = req.body.clockifyToken;
        let url = `${process.env.CLOCKIFY_BASE_ENDPOINT}/workspaces`;
        let options = {
            url: url,
            method: "GET",
            headers: {
                "content-type": "application/json",
                "X-Api-Key": clockifyToken
            }
        };
        return (0, request_1.default)(options, (error, response, body) => {
            let result;
            let data = JSON.parse(body);
            if (data.length > 0) {
                let ws = [];
                data.forEach(workspace => {
                    ws.push(new workspace_1.Workspace(workspace.id, workspace.name));
                });
                result = new WorkSpacesResult_1.WorkspacesResult(ws, new StatusResult_1.StatusResult(StatusResult_1.StatusCode.OK));
            }
            else {
                result = new WorkSpacesResult_1.WorkspacesResult([], new StatusResult_1.StatusResult(StatusResult_1.StatusCode.NO_CONTENT));
            }
            return (0, response_generator_1.default)(res, result.Status, result);
        });
    }
    catch (error) {
        let result = new WorkSpacesResult_1.WorkspacesResult([], new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(error)));
        return (0, response_generator_1.default)(res, result.Status, result);
    }
};
exports.getWorkspaces = getWorkspaces;
const getProjects = (req, res, next) => {
    try {
        let bodyData = req.body;
        let clockifyToken = bodyData.clockifyToken;
        let url = `${process.env.CLOCKIFY_BASE_ENDPOINT}/workspaces/${bodyData.workspaceId}/projects`;
        let query = bodyData;
        let options = {
            url: url,
            method: "GET",
            headers: {
                "content-type": "application/json",
                "X-Api-Key": clockifyToken
            },
            qs: query
        };
        return (0, request_1.default)(options, (error, response, body) => {
            let result;
            if (error) {
                result = new ProjectsResult_1.ProjectsResult([], new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(error)));
                return (0, response_generator_1.default)(res, result.Status, result);
            }
            else {
                let data = JSON.parse(body);
                let projects = [];
                if (data.length > 0) {
                    data.forEach(project => {
                        projects.push(new Project_1.Project(project.id, project.name, project.workspaceId));
                    });
                    result = new ProjectsResult_1.ProjectsResult(projects, new StatusResult_1.StatusResult(StatusResult_1.StatusCode.OK));
                }
                else {
                    result = new ProjectsResult_1.ProjectsResult([], new StatusResult_1.StatusResult(StatusResult_1.StatusCode.NO_CONTENT));
                }
                return (0, response_generator_1.default)(res, result.Status, result);
            }
        });
    }
    catch (error) {
        let result = new WorkSpacesResult_1.WorkspacesResult([], new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(error)));
        return (0, response_generator_1.default)(res, result.Status, result);
    }
};
exports.getProjects = getProjects;
const getReportDetailed = (req, res, next) => {
    try {
        let bodyData = req.body;
        let clockifyToken = bodyData.clockifyToken;
        let url = `${process.env.CLOCKIFY_REPORT_BASE_ENDPOINT}/workspaces/${bodyData.workspaceId}/reports/detailed`;
        delete req.body.clockifyToken;
        delete req.body.workspaceId;
        let clockifyBody = req.body;
        let options = {
            url: url,
            method: "POST",
            headers: {
                "content-type": "application/json",
                "X-Api-Key": clockifyToken
            },
            body: JSON.stringify(clockifyBody)
        };
        return (0, request_1.default)(options, (error, response, body) => {
            let result;
            let report = new Report_1.Report();
            if (error) {
                result = new ReportResult_1.ReportResult(new Report_1.Report(), new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(error)));
                return (0, response_generator_1.default)(res, result.Status, result);
            }
            else {
                let data = JSON.parse(body);
                let timeEntries = [];
                let totales = [];
                if (data && data.totals.length > 0 && data.timeentries.length > 0) {
                    data.totals.forEach((tt) => {
                        totales.push({ _id: tt._id, totalTime: tt.totalTime, totalBillableTime: tt.totalBillableTime, entriesCount: tt.entriesCount, totalAmount: tt.totalAmount });
                    });
                    data.timeentries.forEach((te) => {
                        timeEntries.push(new TimeEntry_1.TimeEntry(te._id, te.description, te.userId, te.billable, te.projectId, te.timeInterval, te.userName, te.userEmail, te.projectName, te.projectColor, te.clientName, te.clientId));
                    });
                    result = new ReportResult_1.ReportResult(new Report_1.Report(totales, timeEntries), new StatusResult_1.StatusResult(StatusResult_1.StatusCode.OK));
                }
                else {
                    result = new ReportResult_1.ReportResult(new Report_1.Report(), new StatusResult_1.StatusResult(StatusResult_1.StatusCode.NO_CONTENT));
                }
                return (0, response_generator_1.default)(res, result.Status, result);
            }
        });
    }
    catch (error) {
        let result = new WorkSpacesResult_1.WorkspacesResult([], new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(error)));
        return (0, response_generator_1.default)(res, result.Status, result);
    }
};
exports.getReportDetailed = getReportDetailed;
