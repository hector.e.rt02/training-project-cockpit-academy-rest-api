"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendEmailNotification = exports.resetPassword = exports.forgotPassword = exports.editMyProfile = exports.registerUser = exports.login = void 0;
const User_1 = require("../models/User");
const class_validator_1 = require("class-validator");
const response_generator_1 = __importDefault(require("../utils/response-generator"));
const StatusResult_1 = require("../models-result/StatusResult");
const util_1 = require("util");
const LoginResult_1 = require("../models-result/LoginResult");
//#region Controller Methods
const login = (req, res, next) => {
    try {
        let userName = req.params["userName"];
        let password = req.params["password"];
        let result;
        if (userName == null || password == null) {
            result = new LoginResult_1.LoginResult(new User_1.User(0, "", "", "", "", "", ""), "", new StatusResult_1.StatusResult(StatusResult_1.StatusCode.BAD_REQUEST, "'userName' and 'password' fields are required."));
        }
        else {
            result = User_1.User.login(userName, password);
        }
        return (0, response_generator_1.default)(res, result.Status, result);
    }
    catch (error) {
        return (0, response_generator_1.default)(res, new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(error)));
    }
};
exports.login = login;
const registerUser = (req, res, next) => {
    let user = req.body;
    return (0, class_validator_1.validate)(user).then((errors) => __awaiter(void 0, void 0, void 0, function* () {
        let result;
        if (errors.length > 0) {
            result = new StatusResult_1.StatusResult(StatusResult_1.StatusCode.BAD_REQUEST, JSON.stringify(errors));
        }
        else {
            result = yield User_1.User.register(user);
        }
        return (0, response_generator_1.default)(res, result);
    })).catch(msj => {
        return (0, response_generator_1.default)(res, new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(msj)));
    });
};
exports.registerUser = registerUser;
const editMyProfile = (req, res, next) => {
    let user = req.body;
    return (0, class_validator_1.validate)(user).then(erros => {
        let result;
        if (erros.length > 0) {
            result = new StatusResult_1.StatusResult(StatusResult_1.StatusCode.BAD_REQUEST, JSON.stringify(erros));
        }
        else {
            result = User_1.User.editMyProfile(user);
        }
        return (0, response_generator_1.default)(res, result);
    }).catch(msj => {
        return (0, response_generator_1.default)(res, new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(msj)));
    });
};
exports.editMyProfile = editMyProfile;
const forgotPassword = (req, res, next) => {
    try {
        let email = req.body.email;
        let result;
        if ((0, util_1.isNullOrUndefined)(email)) {
            result = new StatusResult_1.StatusResult(StatusResult_1.StatusCode.BAD_REQUEST, "The field 'Email' is required.");
        }
        else {
            result = User_1.User.forgotPassword(email);
        }
        return (0, response_generator_1.default)(res, result);
    }
    catch (error) {
        return (0, response_generator_1.default)(res, new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(error)));
    }
};
exports.forgotPassword = forgotPassword;
const resetPassword = (req, res, next) => {
    let data = req.body;
    console.log(`Data: ${JSON.stringify(data)}`);
    return (0, class_validator_1.validate)(data).then(errors => {
        let result;
        if (errors.length > 0) {
            result = new StatusResult_1.StatusResult(StatusResult_1.StatusCode.BAD_REQUEST, JSON.stringify(errors));
        }
        else {
            result = User_1.User.resetPassword(data.UserId, data.OldPassword, data.NewPassword);
        }
        return (0, response_generator_1.default)(res, result);
    }).catch(msj => {
        return (0, response_generator_1.default)(res, new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(msj)));
    });
};
exports.resetPassword = resetPassword;
const sendEmailNotification = (req, res, next) => {
    try {
        let data = req.body;
        if ((0, util_1.isNullOrUndefined)(data)) {
            let responseData = { message: "The fields 'Email' and 'type' are required.", dataRecived: req.body };
            let msj = "The information received is not valid";
            return (0, response_generator_1.default)(res, new StatusResult_1.StatusResult(StatusResult_1.StatusCode.BAD_REQUEST, msj), responseData);
        }
        else {
            return (0, response_generator_1.default)(res, User_1.User.sendEmailNotification(data.email, data.type));
        }
    }
    catch (error) {
        return (0, response_generator_1.default)(res, new StatusResult_1.StatusResult(StatusResult_1.StatusCode.INTERNAL_SERVER_ERROR, JSON.stringify(error)));
    }
};
exports.sendEmailNotification = sendEmailNotification;
//#endregion Controller Methods
