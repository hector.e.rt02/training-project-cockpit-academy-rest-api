"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_config_1 = require("../utils/database-config");
const mssql_1 = __importDefault(require("mssql"));
function excuteScript(sqlScript, parameters = [], isUSP = false) {
    return mssql_1.default.connect(database_config_1.sqlConfig).then(pool => {
        let req = pool.request();
        parameters.forEach(p => {
            req.input(p.name, p.type, p.value);
        });
        if (isUSP) {
            return req.execute(sqlScript);
        }
        else {
            return req.query(sqlScript);
        }
    }).then(result => {
        // console.dir(result)
        return result;
    });
}
exports.default = excuteScript;
