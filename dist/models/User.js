"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = exports.NotificationType = void 0;
const StatusResult_1 = require("../models-result/StatusResult");
const class_validator_1 = require("class-validator");
const LoginResult_1 = require("../models-result/LoginResult");
const database_server_1 = __importDefault(require("../Database/database-server"));
const mssql_1 = __importDefault(require("mssql"));
var NotificationType;
(function (NotificationType) {
    NotificationType[NotificationType["RESET_PASSWORD"] = 1] = "RESET_PASSWORD";
    NotificationType[NotificationType["CONFIRM_ACCOUNT"] = 2] = "CONFIRM_ACCOUNT";
})(NotificationType = exports.NotificationType || (exports.NotificationType = {}));
class User {
    constructor(pUserId, pUserName, pPassword, pName, pEmail, pPhoto, pClockifyToken) {
        this.UserId = pUserId;
        this.UserName = pUserName;
        this.Password = pPassword;
        this.Name = pName;
        this.Email = pEmail;
        this.Photo = pPhoto;
        this.ClockifyToken = pClockifyToken;
    }
    //#region Static Methods
    static register(pUser) {
        // this.UpdatePruebasInfo(pUser,true);
        let par = [];
        par.push({ name: "pName", type: mssql_1.default.VarChar, value: pUser.Name });
        par.push({ name: "pEmail", type: mssql_1.default.VarChar, value: pUser.Email });
        par.push({ name: "pUserName", type: mssql_1.default.VarChar, value: pUser.UserName });
        par.push({ name: "pPassword", type: mssql_1.default.VarChar, value: pUser.Password });
        par.push({ name: "pPhoto", type: mssql_1.default.VarChar, value: pUser.Photo });
        par.push({ name: "pClockifyToken", type: mssql_1.default.VarChar, value: pUser.ClockifyToken });
        return (0, database_server_1.default)("uspUserInsert", par, true).then((result) => {
            let res;
            if (result.recordset.length > 0 && result.recordset[0].UserId) {
                res = new StatusResult_1.StatusResult(StatusResult_1.StatusCode.OK, JSON.stringify({ UserId: +result.recordset[0].UserId }));
            }
            else {
                res = new StatusResult_1.StatusResult(StatusResult_1.StatusCode.NOT_MODIFIED, "We couldn't save your info.");
            }
            return res;
        }).catch(msj => {
            return new StatusResult_1.StatusResult(StatusResult_1.StatusCode.NOT_MODIFIED, JSON.stringify(msj));
        });
    }
    static login(pUserName, pPassword) {
        let result;
        // if (pUserName == pruebas.UserName && pPassword == pruebas.Password) {
        //     result = new LoginResult(pruebas,"hola",new StatusResult(StatusCode.OK))
        // } else {
        //     result = new LoginResult(new User(0,pUserName,pPassword,"","","",""),"",new StatusResult(StatusCode.NO_CONTENT));
        // }
        result = new LoginResult_1.LoginResult(new User(0, pUserName, pPassword, "", "", "", ""), "", new StatusResult_1.StatusResult(StatusResult_1.StatusCode.NO_CONTENT));
        return result;
    }
    static editMyProfile(pUser) {
        // let result:StatusResult;
        // if (pUser.UserId == pruebas.UserId) {        
        //     this.UpdatePruebasInfo(pUser);
        //     result = new StatusResult(StatusCode.OK)
        // } else {            
        //     return new StatusResult(StatusCode.UNAUTHORIZED);
        // }
        // return result;
        return new StatusResult_1.StatusResult(StatusResult_1.StatusCode.UNAUTHORIZED);
    }
    static forgotPassword(pEmail) {
        return new StatusResult_1.StatusResult(StatusResult_1.StatusCode.NO_CONTENT);
    }
    static resetPassword(pUserId, pOldPassword, pNewPassword) {
        // if (pruebas.UserId == pUserId && pruebas.Password == pOldPassword ) {
        //     pruebas.Password = pNewPassword;
        //     return new StatusResult(StatusCode.OK,JSON.stringify(pruebas));
        // } else {
        //     return new StatusResult(StatusCode.NOT_MODIFIED);
        // }
        return new StatusResult_1.StatusResult(StatusResult_1.StatusCode.NOT_MODIFIED);
    }
    static sendEmailNotification(pEmail, pType) {
        return new StatusResult_1.StatusResult(StatusResult_1.StatusCode.NO_CONTENT);
    }
}
__decorate([
    (0, class_validator_1.IsNumber)()
], User.prototype, "UserId", void 0);
__decorate([
    (0, class_validator_1.Length)(3, 15)
], User.prototype, "UserName", void 0);
__decorate([
    (0, class_validator_1.Length)(5, 20)
], User.prototype, "Password", void 0);
__decorate([
    (0, class_validator_1.Length)(5, 200)
], User.prototype, "Name", void 0);
__decorate([
    (0, class_validator_1.Length)(10, 100)
], User.prototype, "Email", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)()
], User.prototype, "Photo", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)()
], User.prototype, "ClockifyToken", void 0);
exports.User = User;
// const pruebas:User = new User(2,"juaniquito01","123456","Juan Perez","juaniquito-el-real@gmail.com","","");
